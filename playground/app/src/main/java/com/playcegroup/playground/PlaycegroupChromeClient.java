package com.playcegroup.playground;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class PlaycegroupChromeClient extends WebChromeClient {
    private Context context;
    private ProgressBar progressBar;
    private WebView popupWebView;
    private int webViewScrollX;
    private int webViewScrollY;
    private WebView webView;

    public PlaycegroupChromeClient(ProgressBar progressBar, Context context) {
        this.context = context;
        this.progressBar = progressBar;
    }

    @Override
    public void onCloseWindow(WebView window) {
        super.onCloseWindow(window);
    }

    @SuppressLint("NewApi")
    @Override
    public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
        webView = view;

        WebView newWebView = new WebView(context);
        newWebView.getSettings().setJavaScriptEnabled(true);
        newWebView.getSettings().setSupportZoom(true);
        newWebView.getSettings().setBuiltInZoomControls(true);
        newWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        newWebView.getSettings().setSupportMultipleWindows(true);
        newWebView.setWebContentsDebuggingEnabled(true);


        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams
                (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        newWebView.setLayoutParams(params);

        view.addView(newWebView);
        WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
        transport.setWebView(newWebView);
        resultMsg.sendToTarget();
        webViewScrollX = view.getScrollX();
        webViewScrollY = view.getScrollY();
        view.scrollTo(0, 0);

        newWebView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        popupWebView = newWebView;
        newWebView.post(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);

                    if(popupWebView != null && popupWebView.getUrl().startsWith("https://kauth.kakao.com/")) {
                        closePopup();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        return true;
    }

    public WebView getPopupWebView() {
        return popupWebView;
    }

    public void closePopup() {
        ((ViewManager)popupWebView.getParent()).removeView(popupWebView);
        popupWebView = null;
        webView.scrollTo(webViewScrollX, webViewScrollY);
    }

    @Override
    public void onProgressChanged(WebView view, int newProgress) {
        super.onProgressChanged(view, newProgress);
        progressBar.setProgress(newProgress);
    }
}

package com.playcegroup.playground;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import java.net.URISyntaxException;

public class PlaycegroupWebViewClient extends WebViewClient {
    private Runnable callback;
    private ProgressBar progressBar;
    private Context context;
    private PlaycegroupChromeClient chromeClient;

    public PlaycegroupWebViewClient(ProgressBar progressBar, Runnable callback, Context context) {
        this.progressBar = progressBar;
        this.callback = callback;
        this.context = context;
    }

    public void setChromeClient(PlaycegroupChromeClient chromeClient) {
        this.chromeClient = chromeClient;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://") && !url.startsWith("javascript:")) {
            Intent intent = null;

            try {
                intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME); //IntentURI처리
                Uri uri = Uri.parse(intent.getDataString());

                context.startActivity(new Intent(Intent.ACTION_VIEW, uri));
                return true;
            } catch (URISyntaxException ex) {
                return false;
            } catch (ActivityNotFoundException e) {
                if ( intent == null )	return false;

                if ( handleNotFoundPaymentScheme(intent.getScheme()) )	return true;

                String packageName = intent.getPackage();
                if (packageName != null) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
                    return true;
                }

                return false;
            }
        } else {
            if(url.contains("/signup/kakaotalk")) {
                if(chromeClient != null) {
                    chromeClient.closePopup();
                }
            }
            if(view.canGoBack()) {
                view.loadUrl(url);
                return true;
            }
            return false;
        }
    }

    protected boolean handleNotFoundPaymentScheme(String scheme) {
        if ( PaymentScheme.ISP.equalsIgnoreCase(scheme) ) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + PaymentScheme.PACKAGE_ISP)));
            return true;
        } else if ( PaymentScheme.BANKPAY.equalsIgnoreCase(scheme) ) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + PaymentScheme.PACKAGE_BANKPAY)));
            return true;
        }

        return false;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        progressBar.setVisibility(View.GONE);
        callback.run();
    }
}
